module.exports = function(app) {
	var User = app.models.QualityUser;
	var Role = app.models.Role;
	var RoleMapping = app.models.RoleMapping;

	var adminEmail = 'admin@admin.fr';

	User.find({where: {email: adminEmail}}, function(err, user){
		if(err) throw err;

		if(user == null){
			User.create([
			{
				username: 'admin', email: 'admin@admin.fr', password:'admin'
			}
			], function(err, users){
				if(err) throw err;

				console.log('Created users: ', users);

				Role.create({
					name: 'admin'
				}, function(err, role){
					if(err) throw err;

					console.log('Created role: ', role);

					role.principals.create({
						principalType: RoleMapping.USER,
						principalId: users[0].id
					}, function(err, principal){
						if (err) throw err;

						console.log('Created principal: ', principal);
					});
				});
			});
		}else{
			console.log("Admin already exist");
		}
	})

	
};