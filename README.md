# Server api pour la gestion des utilisateurs, des séries et des films

##### Addresse du server 
- [Documentation](http://de-coster.fr:3001/explorer)
- addresse de l'api http://de-coster.fr:3001/api/

## Récupération d'un utilisateur

`http://localhost:3000/api/Users`

Resultat :

```
[
  {
    "realm": "string",
    "username": "romain",
    "credentials": null,
    "challenges": null,
    "email": "romain.decoster.rdc@gmail.com",
    "emailVerified": null,
    "verificationToken": null,
    "status": null,
    "created": null,
    "lastUpdated": null,
    "id": 1
  },
  {
    "realm": null,
    "username": "admin",
    "credentials": null,
    "challenges": null,
    "email": "admin@admin.fr",
    "emailVerified": null,
    "verificationToken": null,
    "status": null,
    "created": null,
    "lastUpdated": null,
    "id": 2
  }
]

```

## Connection d'un utilisateur

Pour cela il faut appeler l'adresse `http://localhost:3000/api/Users/login` avec comme paramètre
 
```
{
	"username":"...",
	"password":"..."
}

```

Resultat :

```
{
  "id": "nu3JCU5UYTa3fGx4k1GKFRcrMjzJwUMPQQAMjk79155G1adgwieIbu1ILA3UUFI5",
  "ttl": 1209600,
  "created": "2015-10-17T22:39:07.714Z",
  "userId": 2
}

```
Nous obtenons l'`access_token` qui correspond `id`

## Utilisation du reste de l'api 
Pour utiliser le reste de l'api il faut ajouter l'`access_token`à chaque requête HTTP.

Exemple :

`http://localhost:3000/api/Users/count?access_token=nu3JCU5UYTa3fGx4k1GKFRcrMjzJwUMPQQAMjk79155G1adgwieIbu1ILA3UUFI5`

Resutat :

```
{
  "count": 5
}

```

## Création d'un utilisateur

Pour créer un utilisateur avec lequel il n'y aura pas de problème d'authorisation il faut remplir uniquement les champs :

```
{
	"realm":...,
	"username":...,
	"password":...,
	"email":...,
	"created":...,
	"lastupdated":...
	
}
```	